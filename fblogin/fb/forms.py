from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms


class userlogin(forms.Form):
    username=forms.CharField()
    password=forms.CharField(widget=forms.PasswordInput)





class signupformn(UserCreationForm):
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    email = forms.EmailField(max_length=254,help_text='Required. Infrorm a valid email address')
    birth_date = forms.DateField(help_text='Required. Format: YYYY-MM-DD')
    gender = (('M', 'Male'), ('F', 'Female'))


class meta:
    model = User
    fields = ('username','birth_date','email','gender')