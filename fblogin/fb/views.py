from django.http import HttpResponse

from django.contrib.auth import authenticate, login
# from django.shortcuts import render, redirect
# from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.contrib.auth.models import User



from .models import Profile


def hello(request):
    return render(request, "login.html")

def logins(request):
    if request.method=="POST":

        username=request.POST.get('username')
        password = request.POST.get('password')
        user=authenticate(request,username=username, password=password)
        if user is None:
            print("Wrong Crendential")
        else:
            login(request,user)
            return redirect("/admin")


    return render(request,"login.html")

def signup(request):
    if request.method == 'POST':
        first_name = request.POST.get('fname')
        last_name = request.POST.get('lname')
        Uname=first_name+last_name
        email=request.POST.get('email')
        password1=request.POST.get('pass')
        Day = request.POST.get('day')
        Month = request.POST.get('month')
        Year = request.POST.get('year')
        Date = Year + "-" + Month + "-" + Day
        Gender = request.POST.get('gender')

        user1 = User(username=Uname, first_name=first_name, last_name=last_name, email=email, password=password1)
        user1.set_password(password1)
        user1.save()
        profile1 = Profile(user=user1, birth_date=Date, gender=Gender)
        user = authenticate(request, username=Uname, password=password1)
        profile1.save()
        return render(request, "signup.html")

    return render(request,"signup.html")

def selectUsers(request):
    profiles=User.objects.all()

    context={'profiles':profiles}


    return render(request,"selectUsers.html",context)
