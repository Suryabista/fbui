from django.urls import path
from . views import hello
from .views import logins
from .views import signup
from .views import selectUsers

urlpatterns = [

    path('', logins),
    path('registration',signup),
    path('select', selectUsers),


    # path('admin/', admin.site.urls),

]
